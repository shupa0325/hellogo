package hellogo

// Hello test import
func Hello() string {
	return "hello"
}

type World struct {
}

func (w World) Get() string {
	return "world"
}

type TryWorld interface {
	Get() string
}
